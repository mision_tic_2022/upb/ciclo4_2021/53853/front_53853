import React from 'react'
import { Route, Routes } from 'react-router';
import Auth from '../pages/Auth';
import Home from '../pages/Home';
import NotFound from '../pages/NotFound';

const UnauthRouter = () => {
    return (
        <Routes>
            <Route path="/" element={<Auth/>}/>
            <Route path="/home" element={<Home/>}/>
            <Route path="*" element={<NotFound/>}/>
        </Routes>
    )
}

export default UnauthRouter;
