import React from "react";
import { Table, Button } from "react-bootstrap";

const ProductTable = ({ products, handleEdit }) => {

  
  return (
    <div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Price</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.map((obj, i) => {
            return (
              <tr key={i}>
                <td>{i + 1}</td>
                <td>{obj.name}</td>
                <td>{obj.price}</td>
                <td>
                  <Button variant="warning" onClick={()=>{handleEdit(obj)}}>Update</Button>{" "}
                  <Button variant="danger">Delete</Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

export default ProductTable;
