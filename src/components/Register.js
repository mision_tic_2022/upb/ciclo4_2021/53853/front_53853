import React, { useContext, useState } from "react";
import { Form, Row, Col, Button } from "react-bootstrap";
import { useNavigate } from "react-router";
import AuthContext from "../context/AuthContext";

const objForm = {
    name: "",
    lastname: "",
    email: "",
    password: ""
}

const Register = () => {
    //Estados
    const [form, setForm] = useState(objForm);
    //Contextos
    const {handleRegister} = useContext(AuthContext);

    const handleForm = (e)=>{
        let obj = {...form, [e.target.name]: e.target.value};
        setForm(obj);
    }

    const handleSubmit = (e)=>{
        e.preventDefault();
        handleRegister(form);
        setForm(objForm);
    }

  return (
    <div>
      <h2>Register</h2>
      <Form onSubmit={handleSubmit}>
        {/*******Fila 1******/}
        <Row>
          {/******Columna 1*******/}
          <Col>
            <Form.Group className="mb-3" controlId="registerName">
              <Form.Label>Name</Form.Label>
              <Form.Control value={form.name} onChange={handleForm} name="name" type="text" placeholder="Enter name" />
            </Form.Group>
          </Col>
          {/******Columna 2*******/}
          <Col>
            <Form.Group className="mb-3" controlId="registerLastname">
              <Form.Label>Lastname</Form.Label>
              <Form.Control value={form.lastname} onChange={handleForm} name="lastname" type="text" placeholder="Enter lastname" />
            </Form.Group>
          </Col>
        </Row>

        {/*******Fila 2******/}
        <Row>
          {/******Columna 1*******/}
          <Col>
            <Form.Group className="mb-3" controlId="registerEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control value={form.email} onChange={handleForm} name="email" type="email" placeholder="Enter Email" />
            </Form.Group>
          </Col>
          {/******Columna 2*******/}
          <Col>
            <Form.Group className="mb-3" controlId="registerPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control value={form.password} onChange={handleForm} name="password" type="password" placeholder="Enter password" />
            </Form.Group>
          </Col>
        </Row>
        {/******Botón de registro********/}
        <Button variant="primary" type="submit">Register</Button>
      </Form>
    </div>
  );
};

export default Register;
