import React, {useEffect, useState} from "react";
import { Modal, Button, Form, Row, Col } from "react-bootstrap";

const objForm = {
    name: "",
    price: ""
  };

const ProductFormModal = ({ show, handleClose, handleUpdate, product}) => {
  
  const [form, setForm] = useState(objForm);

  useEffect(()=>{
      console.table(product);
      setForm({id: product._id, name: product.name, price: product.price});
  }, [product.name]);

  const handleForm = (e) => {
    let obj = { ...form, [e.target.name]: e.target.value };
    setForm(obj);
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Update product</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Row>
          {/**Columna para el nombre del producto */}
          <Col>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="name">Name</Form.Label>
              <Form.Control
                value={form.name}
                onChange={handleForm}
                name="name"
                id="name"
                type="text"
                placeholder="Enter name"
              />
            </Form.Group>
          </Col>
          {/**Columna para el precio del producto */}
          <Col>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="price">Price</Form.Label>
              <Form.Control
                value={form.price}
                onChange={handleForm}
                name="price"
                id="price"
                type="number"
                placeholder="Enter price"
              />
            </Form.Group>
          </Col>
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={()=>handleUpdate(form)}>
          Update
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ProductFormModal;
