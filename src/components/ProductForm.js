import React, { useState } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

const ProductForm = ({handleProducts}) => {
    const objForm = {
        name: '',
        price: ''
    }
    const [form, setForm] = useState(objForm);

    const handleForm = (e)=>{
        let obj = {...form, [e.target.name]: e.target.value};
        setForm(obj)
    }

    const handleSubmit = (e)=>{
        e.preventDefault();
        handleProducts(form);
        setForm(objForm);
    }

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <Row>
          {/**Columna para el nombre del producto */}
          <Col>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="name">Name</Form.Label>
              <Form.Control value={form.name} onChange={handleForm} name="name" id="name" type="text" placeholder="Enter name" />
            </Form.Group>
          </Col>
          {/**Columna para el precio del producto */}
          <Col>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="price">Price</Form.Label>
              <Form.Control value={form.price} onChange={handleForm} name="price" id="price" type="number" placeholder="Enter price" />
            </Form.Group>
          </Col>
        </Row>

        <Button variant="primary" type="submit">
          Create
        </Button>
      </Form>
    </div>
  );
};

export default ProductForm;
