import React, { useContext, useState } from "react";
import { Form, Button, Alert } from "react-bootstrap";
import AuthContext from "../context/AuthContext";
import "./Components.css";

const objForm = {
  email: "",
  password: "",
};

const Login = () => {
  //Contextos
  const { handleLogin } = useContext(AuthContext);
  //Estados
  const [form, setForm] = useState(objForm);
  const [show, setShow] = useState(false);

  const handleForm = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    let resp = await handleLogin(form);
    if (resp.status === 200) {
      let json = await resp.json();
      localStorage.setItem("token", json.token);
    } else {
        setShow(true);
    }
  };

  return (
    <div className="login">
      <h2>Login</h2>
      <Alert show={show} variant="danger">
        Invalid credentials
      </Alert>
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="loginEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            value={form.email}
            onChange={handleForm}
            name="email"
            required
            type="email"
            placeholder="name@example.com"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="loginPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            value={form.password}
            onChange={handleForm}
            name="password"
            required
            type="password"
            placeholder="Enter password"
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Enter
        </Button>
      </Form>
    </div>
  );
};

export default Login;
