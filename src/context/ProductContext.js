import { createContext } from "react";
import { apiProduct } from "./Api";

const ProductContext = createContext();

const ProductProvider = ({ children }) => {
  //Función para enviar petición al servidor
  const handleRegister = async (objProduct) => {
    //Acceder al token que se encuentra en localStorage
    const token = localStorage.getItem("token");
    //Api fetch con metodo post
    const resp = await fetch(apiProduct, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(objProduct),
    });
    return resp;
  };

  const getProducts = async () => {
    //Obtener el token
    const token = localStorage.getItem("token");
    //Enviar petición al servidor
    const resp = await fetch(apiProduct, {
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          }
    });
    return resp;
  };

  const setProduct = async(objProduct)=>{
    //Obtener el token
    const token = localStorage.getItem("token");
    let resp = await fetch(apiProduct, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(objProduct)
    });
    return resp;
  }

  const data = { handleRegister, getProducts, setProduct };

  return (
    <ProductContext.Provider value={data}>{children}</ProductContext.Provider>
  );
};

export { ProductProvider };
export default ProductContext;
