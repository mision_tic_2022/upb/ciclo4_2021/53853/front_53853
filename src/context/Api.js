const server = "http://localhost:3000";
const apiRegister = `${server}/user`;
const apiLogin = `${server}/user/auth`;
const apiProduct = `${server}/product`;

export {apiRegister, apiLogin, apiProduct};