import React from 'react'
import { Link } from 'react-router-dom';

const Home = () => {
    return (
        <div>
            <Link to="/">Auth</Link>
            <h2>Home</h2>
        </div>
    )
}

export default Home;
