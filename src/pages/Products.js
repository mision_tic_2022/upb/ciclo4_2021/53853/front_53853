import { useContext, useEffect, useState } from "react";
import ProductForm from "../components/ProductForm";
import ProductTable from "../components/ProductTable";
import { Alert, Button } from "react-bootstrap";
import ProductContext from "../context/ProductContext";
import ProductFormModal from "../components/ProductFormModal";


const Products = () => {
  //Uso del contexto
    const {handleRegister, getProducts, setProduct} = useContext(ProductContext);
  //Estados
  const [products, setProducts] = useState([]);
  const [show, setShow] = useState(false);
  const [alert, setAlert] = useState({ variant: "", msg: "" });
  const [showModal, setShowModal] = useState(false);
  const [productUpdate, setProductUpdate] = useState({_id: "", name: "", price: ""});

  //Ejecutar función al cargar el componente (hook)
  useEffect(()=>{
    getProducts().then(async (resp)=>{
        let json = await resp.json();
        setProducts(json);
    });
  }, []);

  const handleProducts = (objProducts) => {
    //Utilizar función del contexto
    handleRegister(objProducts).then(async (resp) => {
        if (resp.status === 201) {
          let json = await resp.json();
          //Crear array con nuevo objeto 'product'
          let array = [...products, json];
          setProducts(array);
          setShow(true);
          setAlert({ variant: "success", msg: "Create" });
        } else {
          setShow(true);
          setAlert({ variant: "danger", msg: "Error" });
        }
      })
      .catch((error) => {
        setShow(true);
        setAlert({ variant: "danger", msg: "Error" });
      });
  };


  const handleEdit = (objProduct)=>{
    //console.table(objProduct);
    setProductUpdate(objProduct);
    setShowModal(true);
  }

  const handleCloseModal = ()=>{
    setShowModal(false);
  }

  const handleUpdate = async (objProduct)=>{
    let resp = await setProduct(objProduct);
    if(resp.status === 200){
      setAlert({ variant: "success", msg: "Producto actualizado" });
      setShow(true);
      setTimeout(()=>setShow(false), 3000);
      setShowModal(false);
    }
  }

  return (
    <>
      <h2>Products</h2>
      <ProductForm handleProducts={handleProducts} />
      <br />
      <Alert variant={alert.variant} show={show}>
        {alert.msg}
        <div className="d-flex justify-content-end">
          <Button onClick={() => setShow(false)} variant="outline-success">
            Close
          </Button>
        </div>
      </Alert>
      <br />
      <ProductTable products={products} handleEdit={handleEdit} />
      {/*****Llamada al modal***********/}
      <ProductFormModal show={showModal} handleClose={handleCloseModal} handleUpdate={handleUpdate} product={productUpdate}/>
    </>
  );
};

export default Products;
