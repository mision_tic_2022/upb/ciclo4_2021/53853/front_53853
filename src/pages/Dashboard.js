import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { Link, Outlet } from "react-router-dom";
import logo from "../logo.svg";

const Dashboard = () => {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home">
            <img
              alt=""
              src={logo}
              width="30"
              height="30"
              className="d-inline-block align-top"
            />{" "}
            React Bootstrap
          </Navbar.Brand>
          {/*****Opciones de navegación*****/}
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/" href="#home">Home</Nav.Link>
              <Nav.Link as={Link} to="/catalogue" href="#catalogue">Catalogue</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      {/**********Aquí se renderizan las rutas anidadas de dashboard********/}
      <Outlet/>
    </>
  );
};

export default Dashboard;
